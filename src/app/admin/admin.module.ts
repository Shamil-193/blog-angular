import { CommonModule } from "@angular/common";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatButtonModule } from "@angular/material/button";

import { LoginPageComponent } from "./login-page/login-page.component";
import { AdminLayoutComponent } from "./shared/components/admin-layout/admin-layout.component";
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { CreatePageComponent } from './create-page/create-page.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { SharedModule } from "../shared/shared.module";
import { AuthGuard } from "./shared/services/auth.guard";
import { MatPaginatorModule } from "@angular/material/paginator";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import { SearchPipe } from "./shared/search.pipe";
import { AlertComponent } from './shared/components/alert/alert.component';
import { AlertService } from "./shared/services/alert.service";

@NgModule({
    declarations: [
        AdminLayoutComponent,
        LoginPageComponent,
        DashboardPageComponent,
        CreatePageComponent,
        EditPageComponent,
        SearchPipe,
        AlertComponent 
    ],
    imports:[
        CommonModule,
        SharedModule, 
        FormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        ReactiveFormsModule,
        MatButtonModule,
        RouterModule.forChild([{
            path:"", component: AdminLayoutComponent, children:[
                {path:"", redirectTo:"/admin/login", pathMatch:"full"},
                {path: "login", component: LoginPageComponent},
                {path:"dashboard", component:DashboardPageComponent, canActivate:[AuthGuard]},
                {path:"create", component:CreatePageComponent, canActivate:[AuthGuard]},
                {path:"post/:id/edit", component:EditPageComponent, canActivate:[AuthGuard] }
            ]
        }])
    ],
    providers:[
        AuthGuard,
        AlertService],
    exports:[RouterModule],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
})
export class AdminModule{

}