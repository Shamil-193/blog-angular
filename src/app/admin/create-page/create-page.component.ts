import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Post } from 'src/app/shared/interfaces/intefaces';
import { PostsService } from 'src/app/shared/posts.servise';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit {

  form: FormGroup

  constructor(private postsService: PostsService,
              private alertService: AlertService) { }

  ngOnInit(): void {
    this.form=new FormGroup({
      title:new FormControl(null,[Validators.required, Validators.minLength(3)]),
      text:new FormControl(null,[Validators.required]),
      author:new FormControl(null,[Validators.required]),
    })
  }

  submit(){
    if(this.form.invalid){
     return
    }

    const post: Post={
      title: this.form.value.title,
      text: this.form.value.text,
      author: this.form.value.author, 
      date: new Date(),
      id: ""
     }

     this.postsService.create(post).subscribe(()=>{
      this.form.reset()
      this.alertService.success("Пост создан")
     })
  } 

}