import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertType } from 'src/app/shared/interfaces/intefaces';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {

  @Input() delay = 3000

  public text: string
  public type: AlertType
  aSub: Subscription

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.aSub=  this.alertService.alert$.subscribe(alert=>{
      this.text=alert.text
      this.type=alert.type 

      const timeout=setTimeout(()=>{
        clearTimeout(timeout)
        this.text=""
      }, this.delay)
    })
  }

  ngOnDestroy(): void {
    if(this.aSub){
      this.aSub.unsubscribe()
    }
  }

}
