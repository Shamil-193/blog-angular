import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, Observable, Subject, tap, throwError } from "rxjs";
import { FbAuthResponse, User } from "src/app/shared/interfaces/intefaces";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn:"root"
})
export class AuthService{

    public error$: Subject<string>=new Subject<string>( )

    constructor(private http: HttpClient){
    }

    get token(){
        const expDate=new Date(localStorage.getItem("fb-token-exp")!)
        if(new Date>expDate){
            this.logout()
            return null
        }else{
            return localStorage.getItem("fb-token")
        }
    }

    login(user: User): Observable<any>{ 
        user.returnSecureToken=true
        return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
            .pipe(
                tap(this.setToken as any),
                catchError(this.handleError.bind(this) as any)
            )
    }
 
    handleError(error: HttpErrorResponse){
        const {message}=error.error.error
        
        switch(message){
            case "INVALID_EMAIL":
                this.error$.next("Неверный email")
                break
            case "INVALID_PASSWORD":
                this.error$.next("Неверный пароль")
                break
            case "EMAIL_NOT_FOUND":
                this.error$.next("Данный email не зарегистрирован")
                break
        }
        return throwError(error )
    }

    logout(){
        localStorage.clear()
    }

    isAuthenticated():boolean{
        return !!this.token
    }

    setToken(response: FbAuthResponse){
            const expDate=new Date(new Date().getTime() + +response.expiresIn*1000)
            localStorage.setItem("fb-token", response.idToken)
            localStorage.setItem("fb-token-exp", expDate.toString())
    }
    
}