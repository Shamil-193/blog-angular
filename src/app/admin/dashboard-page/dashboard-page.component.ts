import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/shared/interfaces/intefaces';
import { PostsService } from 'src/app/shared/posts.servise';
import { AlertService } from '../shared/services/alert.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit, OnDestroy {

  posts: Post[] = []
  postsSub: Subscription
  delSub: Subscription
  download: boolean=false
  searchStr=""

  constructor(private postsService: PostsService,
              private alertService: AlertService)
  {}

  ngOnInit(): void {
    this.download=true
    this.postsSub=this.postsService.getAll().subscribe(posts=>{
      this.posts=posts
      this.download=false
    }, err =>{
      this.download=false
    })
  }

  ngOnDestroy(): void {
     if(this.postsSub){
      this.postsSub.unsubscribe( )
     }
     if(this.delSub){
      this.delSub.unsubscribe()
     }
  }

  remove(id:string){
    this.delSub=this.postsService.remove(id).subscribe(()=>{
      this.posts=this.posts.filter(post => post.id !== id)
      this.alertService.danger("Пост удален!")
    })
  }

}
