import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, CanActivate, Params } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { Post } from '../shared/interfaces/intefaces';
import { PostsService } from '../shared/posts.servise';

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss']
})
export class PostPageComponent implements OnInit {

  @Input() post: Post

  post$: Observable<Post>

  constructor(
    private route: ActivatedRoute,
    private postService: PostsService
  ) {}

  ngOnInit(): void {
    this.post$ = this.route.params
      .pipe(switchMap((params: Params)=>{
        return this.postService.getById(params["id"])
      }))
  }

}
